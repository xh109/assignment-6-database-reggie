public class Course extends supportDB {


	Course(String name, int credits) {
		super(name, credits);
	}

	public int getCredits() {
		return credits;
	}

	public String getName() {
		return name;
	}
}
