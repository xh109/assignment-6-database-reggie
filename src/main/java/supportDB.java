import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class supportDB {
    static String dbName = " reggie";
    static String connectionURL = "jdbc:derby:" + dbName + ";create=true";
    protected String name;
    protected int credits;
    protected int id;
    protected Course course;
    protected String daysTimes;
    ArrayList<Offering> schedule = new ArrayList<>();

    public supportDB(String name, int credits) {
        this.name = name;
        this.credits = credits;
    }

    public supportDB(int id, Course course, String daysTimesCsv) {
        this.id = id;
        this.course = course;
        this.daysTimes = daysTimesCsv;
    }

    public supportDB(String name) {
        this.name = name;
    }

    public static Connection getConn() throws SQLException{
        Connection conn = null;
        try{
            conn = DriverManager.getConnection(connectionURL);
        }catch(SQLException error) {
            error.printStackTrace();
        }

        return conn;
    }

    public static void executeSQL(String sqlStatement) throws SQLException {
        Connection conn = null;
        try {
            conn = getConn();
            Statement statement = conn.createStatement();
            statement.executeUpdate(sqlStatement);
        }catch(SQLException error){
            error.printStackTrace();
        }finally {
            conn.close();
        }
    }

    public static ResultSet select(String sqlStatement) throws SQLException {
        Connection conn = null;
        ResultSet result = null;
        try {
            conn = getConn();
            Statement statement = conn.createStatement();
            result = statement.executeQuery(sqlStatement);
        }catch(SQLException error){
            error.printStackTrace();
        }
        return result;
    }

    public static Course createCourse(String name, int credits) throws Exception {
        Connection conn = null;

        //try {
            /*conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();*/

            /*//(2)extract excuteSQL
            //(1)extract connection
            conn = getConn();
            Statement statement = conn.createStatement();

            statement.executeUpdate("DELETE FROM course WHERE name = '" + name + "'");
            statement.executeUpdate("INSERT INTO course VALUES ('" + name + "', " + credits
                    + ")");
            */

            executeSQL("DELETE FROM course WHERE name = '" + name + "'");
            executeSQL("INSERT INTO course VALUES ('" + name + "', " + credits
                    + ")");

            return new Course(name, credits);
        /*} finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }*/
    }

    public static Schedule createSchedule(String name) throws Exception {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();

            statement.executeUpdate("DELETE FROM schedule WHERE name = '" + name + "'");
            return new Schedule(name);
        } finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }
    }

    public static Course findCourse(String name) {
        Connection conn = null;
        try {
            /*(3) Extract select;
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM course WHERE name = '" + name
                    + "'");
            */

            ResultSet result = select("SELECT * FROM course WHERE name = '" + name
                    + "'");

            if (!result.next())
                return null;

            int credits = result.getInt("Credits");
            return new Course(name, credits);
        } catch (Exception ex) {
            return null;
        } finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }
    }

    public static Offering createOffering(Course course, String daysTimesCsv) throws Exception {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();

            ResultSet result = statement.executeQuery("SELECT MAX(id) FROM offering");
            result.next();
            int newId = 1 + result.getInt(1);

            statement.executeUpdate("INSERT INTO offering VALUES (" + newId + ",'"
                    + course.getName() + "','" + daysTimesCsv + "')");
            return new Offering(newId, course, daysTimesCsv);
        } finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }
    }

    public static Offering findOffering(int id) {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM offering WHERE id =" + id
                    + "");
            if (result.next() == false)
                return null;

            String courseName = result.getString("name");
            Course course = supportDB.findCourse(courseName);
            String dateTime = result.getString("daysTimes");
            conn.close();

            return new Offering(id, course, dateTime);
        } catch (Exception ex) {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
            return null;
        }
    }

    public static Schedule findSchedule(String name) {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM schedule WHERE name= '" + name
                    + "'");

            Schedule schedule = new Schedule(name);

            while (result.next()) {
                int offeringId = result.getInt("offeringId");
                Offering offering = supportDB.findOffering(offeringId);
                schedule.add(offering);
            }

            return schedule;
        } catch (Exception ex) {
            return null;
        } finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }
    }

    public static Collection<Schedule> all() throws Exception {
        ArrayList<Schedule> result = new ArrayList<Schedule>();
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();
            ResultSet results = statement.executeQuery("SELECT DISTINCT name FROM schedule ORDER BY name");

            while (results.next())
                result.add(Schedule.findSchedule(results.getString("name")));
        } finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }

        return result;
    }


    public void update() throws Exception {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionURL);
            Statement statement = conn.createStatement();

            statement.executeUpdate("DELETE FROM schedule WHERE name = '" + name + "'");

            for (int i = 0; i < schedule.size(); i++) {
                Offering offering = (Offering) schedule.get(i);
                int resultCount = statement.executeUpdate("INSERT INTO schedule VALUES('" + name + "',"
                        + offering.getId() + ")");
            }
        } finally {
            try {
                conn.close();
            } catch (Exception ignored) {
            }
        }
    }
}