import java.util.*;
import java.sql.*;

public class Schedule extends supportDB {
	int credits = 0;
	static final int minCredits = 12;
	static final int maxCredits = 18;
	boolean overloadAuthorized = false;


	public static void deleteAll() throws Exception {
		Connection conn = null;

		try {
			conn = DriverManager.getConnection(connectionURL);
			Statement statement = conn.createStatement();

			statement.executeUpdate("DELETE FROM schedule");
		} finally {
			try {
				conn.close();
			} catch (Exception ignored) {
			}
		}
	}

	public Schedule(String name) {
		super(name);
		this.name = name;
	}

	public void add(Offering offering) {
		credits += offering.getCourse().getCredits();
		schedule.add(offering);
	}

	public void authorizeOverload(boolean authorized) {
		overloadAuthorized = authorized;
	}

	public List<String> analysis() {
		ArrayList<String> result = new ArrayList<String>();

		if (credits < minCredits)
			result.add("Too few credits");

		if (credits > maxCredits && !overloadAuthorized)
			result.add("Too many credits");

		checkDuplicateCourses(result);

		checkOverlap(result);

		return result;
	}

	public void checkDuplicateCourses(ArrayList<String> analysis) {
		HashSet<Course> courses = new HashSet<Course>();
		for (int i = 0; i < schedule.size(); i++) {
			Course course = ((Offering) schedule.get(i)).getCourse();
			if (courses.contains(course))
				analysis.add("Same course twice - " + course.getName());
			courses.add(course);
		}
	}

	public void checkOverlap(ArrayList<String> analysis) {
		HashSet<String> times = new HashSet<String>();

		for (Iterator<Offering> iterator = schedule.iterator(); iterator.hasNext();) {
			Offering offering = (Offering) iterator.next();
			String daysTimes = offering.getDaysTimes();
			StringTokenizer tokens = new StringTokenizer(daysTimes, ",");
			while (tokens.hasMoreTokens()) {
				String dayTime = tokens.nextToken();
				if (times.contains(dayTime))
					analysis.add("Course overlap - " + dayTime);
				times.add(dayTime);
			}
		}
	}

	public String toString() {
		return "Schedule " + name + ": " + schedule;
	}
}
